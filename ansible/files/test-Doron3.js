#!/usr/bin/env nodejs

//const PORT = process.env.PORT;

var http = require('http');
http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.end('service3\n');
}).listen(8083, 'localhost');
console.log('Server running at http://localhost:8083 - service3/');
